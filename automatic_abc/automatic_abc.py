__author__="Tom Charnock"
__version__="0.1"

import numpy as np
import sys
import matplotlib.pyplot as plt
import tqdm.notebook as tqdm
from scipy.stats import gaussian_kde
from histogram_distribution.histogram_distribution.histogram_distribution import HistogramDistribution

class AutomaticABC():
    def __init__(self, target_data, prior, simulator, compression=None, 
                 distance_measure=None):
        self.prior = prior
        self.prior_limits = np.vstack([self.prior.low.numpy(), self.prior.high.numpy()]).T
        self.simulator = simulator
        if compression is None:
            self.compression = lambda x: x
        else:
            self.compression = compression
        self.target_summary = compression(target_data)
        self.n_summaries = self.target_summary.shape[-1]
        if distance_measure is None:
            self.distance_measure = lambda x : np.sqrt(
                np.sum(
                    np.square(
                        self.target_summary - x),
                    -1))
        else:
            self.distance_measure = distance_measure
        self.low = prior.low.numpy().astype(np.float64)
        self.high = prior.high.numpy().astype(np.float64)
        self.n_params = prior.batch_shape[0]
        self.abc_proposals = np.zeros((0, self.n_params))
        self.abc_summaries = np.zeros((0, self.n_summaries))
        self.abc_distances = np.zeros((0,))
        self.accepted = np.zeros((0, self.n_params))
        self.num_accepted = np.zeros((0,))
        self.ϵ = np.zeros((0,))
        self.rel_entropy = np.zeros((0,))
        
    def get_simulations(self, n_simulations):
        abc_proposals = self.prior.sample(n_simulations)
        abc_summaries = self.compression(self.simulator(abc_proposals))
        abc_distances = self.distance_measure(abc_summaries)
        self.abc_proposals = np.concatenate([self.abc_proposals, abc_proposals])
        self.abc_summaries = np.concatenate([self.abc_summaries, abc_summaries])
        self.abc_distances = np.concatenate([self.abc_distances, abc_distances])

    def get_accepted(self, ϵ, return_accepted=False):
        if not return_accepted:
            self.accepted = self.abc_proposals[self.abc_distances <= ϵ]
        else:
            return self.abc_proposals[self.abc_distances <= ϵ]
        
    def calculate_rel_entropy(self, ϵ, previous_dist, mc_samples=None):
        current_proposals = self.get_accepted(ϵ, return_accepted=True)
        current_dist = HistogramDistribution(
            current_proposals, 
            self.prior_limits)
        if mc_samples is not None:
            samples = current_dist.sample(mc_samples)
        else:
            samples = current_proposals
        current_prob = current_dist.prob(samples)
        previous_log_prob = previous_dist.log_prob(samples)
        return current_proposals.shape[0], np.sum(current_prob * (np.log(current_prob) - previous_log_prob)), current_dist
        
    def get_rel_entropy(self, from_prior=True, min_samples=1000, rate=0.95, ϵ_init=None, n_simulations=None, patience=10,
                        max_resamples=50, mc_samples=None, return_stats=False, in_order=False, start=None, skip=None):
        self.check_existing(data="distances", plot=False)
        if in_order:
            ϵs = np.sort(self.abc_distances)[::-1]
            if ϵ_init is not None:
                if start is not None:
                    print("Using `ϵ_init` rather than `start` index")
                start = np.argmin(np.abs(ϵs - ϵ_init))
            elif start is None:
                start = 0
            if skip is None:
                skip = 1
            ϵs = ϵs[start::skip]
        else:
            if ϵ_init is None:
                ϵ_init = self.abc_distances.max()
            ϵs = [ϵ_init]
        ϵ = ϵs[0]    
        initial_proposals = self.get_accepted(ϵ, return_accepted=True)
        previous_dist = HistogramDistribution(
            initial_proposals, 
            self.prior_limits)
        current_accepted = initial_proposals.shape[0]
        num_accepted = []
        rel_entropy = []
        drel_entropy = []
        bar = tqdm.tqdm(desc="Iterations")
        resample_counter = 0
        counter = 0
        patience_counter = 0
        max_rel_entropy = 0
        while current_accepted >= min_samples:
            if resample_counter >= max_resamples:
                print("Resampled {} times".format(resample_counter+1))
                print("At turn over ϵ = {}".format(ϵs[np.argmin(np.abs(np.array(rel_entropy) - max_rel_entropy))]))
                break
            if in_order:
                if counter+1 == ϵs.shape:
                    break
                current_ϵ = ϵs[counter+1]
            else:
                current_ϵ = ϵ * rate
                ϵs.append(current_ϵ)
            current_accepted, current_rel_entropy, current_dist = self.calculate_rel_entropy(
                current_ϵ, previous_dist, mc_samples=mc_samples)
            num_accepted.append(current_accepted)
            rel_entropy.append(current_rel_entropy)
            if (patience_counter >= patience) and (n_simulations is not None):
                counter -= patience + 1 
                patience_counter = 0
                drel_entropy = drel_entropy[:counter-1]
                num_accepted = num_accepted[:counter]
                rel_entropy = rel_entropy[:counter]
                ϵs = ϵs[:counter]
                current_ϵ = ϵs[-1]
                print("Not enough simulations... doing more", end="\r")
                self.get_simulations(n_simulations)  
                current_accepted_proposals = self.get_accepted(ϵ_init, return_accepted=True)
                current_accepted = current_accepted_proposals.shape[0]
                if not from_prior:
                    current_dist = HistogramDistribution(
                        current_accepted_proposals, 
                        self.prior_limits)
                if in_order:
                    additional = np.sort(self.abc_distances[self.abc_distances <= current_ϵ])[::-1]
                    if skip is not None:
                        additional = additional[::skip]
                    ϵs = np.hstack([ϵs, additional])
                resample_counter += 1
            elif (patience_counter >= patience): 
                ϵs = ϵs[:counter] 
                print("Distribution diverging for {} steps".format(patience))
                print("At turn over ϵ = {}".format(ϵs[counter - patience]))
                break
            elif rel_entropy[-1] >= max_rel_entropy:
                if not np.isinf(rel_entropy[-1]):
                    max_rel_entropy = rel_entropy[-1]
                    patience_counter = 0
                else:
                    current_ϵ = ϵs[counter-1]
                    patience_counter += 1
            elif patience_counter <= patience:
                patience_counter += 1
            else:
                print("I've fogotten a condition")
                break
                
            if not in_order:
                ϵ = current_ϵ
            if not from_prior:
                previous_dist = current_dist
            counter += 1
            postfix_dict = {
                "rel entropy":rel_entropy[-1],
                "num accepted":num_accepted[-1],
                "ϵ":current_ϵ,
                "resampled": resample_counter,
                "patience": patience_counter,
                "epsilon length": len(ϵs),
                "counter": counter
            }
            if len(rel_entropy) > 1: 
                drel_entropy.append(rel_entropy[-1] - rel_entropy[-2])
                postfix_dict["d (rel entropy)"] = drel_entropy[-1]
            bar.set_postfix(postfix_dict)
            bar.update()
        if in_order:
            ϵs = ϵs[:counter-1]
        ϵs = np.array(ϵs)
        if not return_stats:
            self.ϵ = ϵs
            self.num_accepted = np.array(num_accepted)
            self.rel_entropy = np.array(rel_entropy)
            self.drel_entropy = np.array(drel_entropy)
        else:
            return ϵs, np.array(num_accepted), np.array(rel_entropy), np.array(drel_entropy)
        
    def mirror(self, data, low, high):
        if len(data.shape) == 1:
            temp_data = np.expand_dims(np.copy(data), -1)
            squeeze = True
        else:
            temp_data = np.copy(data)
            squeeze = False
        if (type(low) is not list) and (type(low) is not np.ndarray):
            low = [low]
        if (type(high) is not list) and (type(high) is not np.ndarray):
            high = [high]
        mirror_points = np.repeat(temp_data[np.newaxis], 2 * temp_data.shape[-1], axis=0)
        for i in range(temp_data.shape[-1]):
            mirror_points[2 * i, ..., i] = low[i] - np.abs((mirror_points[2 * i, ..., i] - low[i]))
            mirror_points[(2 * i) + 1, ..., i] = high[i] + (high[i] - mirror_points[(2 * i) + 1, ..., i])
        if squeeze:
            return np.squeeze(np.vstack([temp_data, mirror_points.reshape((-1,) + temp_data.shape[1:])]), -1)
        else:
            return np.vstack([temp_data, mirror_points.reshape((-1,) + temp_data.shape[1:])])
                                 
    def check_existing(self, data, plot=True):
        if data == "accepted":
            if self.accepted.shape[0] != 0:
                size = self.n_params
                data = self.accepted
            else:
                print("there are no accepted samples to plot. run `abc.get_simulations(n_simulations); abc.get_accepted(ϵ)`")
                sys.exit()
        elif data == "parameters":
            if self.abc_proposals.shape[0] != 0:
                size = self.n_params
                data = self.abc_proposals
            else:
                print("there are no proposed samples to plot. run `abc.get_simulations(n_simulations)`")
                sys.exit()
        elif data == "summaries":
            if self.abc_summaries.shape[0] != 0:
                size = self.n_summaries
                data = self.abc_summaries
            else:
                print("there are no collected summaries to plot. run `abc.get_simulations(n_simulations)`")
                sys.exit()
        elif data == "distances":
            if plot:
                print("Cannot plot a corner plot of distances")
                sys.exit()
            if self.abc_distances.shape[0] != 0:
                return
            else:
                print("there are no distances calculated. run `abc.get_simulations(n_simulations)`")
                sys.exit()
        elif type(data) == np.ndarray:
            size = data.shape[-1]
        else:
            print("`data` must be either 'accepted', parameters', 'summaries', or a numpy array containing the data")
            sys.exit()
        return data, size         
                                  
    def corner_plot(self, data="accepted", labels=None, scale=4, bins=25, gridsize=100, fontsize=16, filename=None, kde=False, scatter=False, mirror=False): 
        data, size = self.check_existing(data)
        if labels is None:
            labels = ["" for i in range(size)]
        if type(labels) is not list:
            print("`labels` should be a list")
            sys.exit()
        if len(labels) != self.n_params:
            print("`labels` needs to be a list with the same dimension as the number of parameters. " +
                  "Number of parameters is {} and the number of labels are {}".format(size, len(labels)))
            sys.exit()
        columns = size
        rows = size
        fig, ax = plt.subplots(
            rows, 
            columns, 
            figsize=(scale * columns, scale * rows))
        plt.subplots_adjust(wspace=0.1, hspace=0.1)
        for column in range(columns):
            xrange = np.linspace(
                self.low[column], 
                self.high[column],
                gridsize)
            for row in range(rows):
                yrange = np.linspace(
                    self.low[row], 
                    self.high[row], 
                    gridsize)
                if row < column:
                    ax[row, column].axis("off")
                else:
                    ax[row, column].set_xlim(xrange[0], xrange[-1])
                    if row < rows - 1:
                        ax[row, column].set_xticks([])
                    else:
                        ax[row, column].set_xlabel(labels[column])
                    if column > 0:
                        ax[row, column].set_yticks([])
                    else:
                        if row > 0:
                            ax[row, column].set_ylabel(labels[row])
                    if row == column:
                        this_data = data[..., row].flatten()
                        ax[row, column].hist(
                            this_data, 
                            bins=bins, 
                            range=[xrange[0], xrange[-1]],
                            density=True)
                        if kde:
                            if mirror:
                                this_data = self.mirror(
                                    this_data, 
                                    self.low[row], 
                                    self.high[row])
                            dist = gaussian_kde(
                                this_data)(xrange)
                            if mirror:
                                dist = dist * 3.
                        else:
                            dist = HistogramDistribution(
                                np.expand_dims(
                                    this_data, 
                                    -1), 
                                np.array([[
                                    self.low[row], 
                                    self.high[row]]])).prob(
                                np.expand_dims(xrange, -1))
                        ax[row, column].plot(xrange, dist)
                    else:
                        ax[row, column].set_ylim(yrange[0], yrange[-1])
                        this_data = data[..., (row, column)].reshape((-1, 2))
                        X, Y = np.meshgrid(xrange, yrange)
                        if kde:
                            if mirror:
                                this_data = self.mirror(
                                    this_data, 
                                    self.low[[row, column]], 
                                    self.high[[row, column]])
                            dist = gaussian_kde(
                                this_data.T)(
                                    np.vstack([
                                        Y.ravel(), 
                                        X.ravel()])).reshape((
                                gridsize, 
                                gridsize))
                        else:
                            dist = HistogramDistribution(
                                this_data, 
                                np.array([[self.low[row], self.high[row]], 
                                          [self.low[column], self.high[column]]])).prob(
                                np.vstack(
                                    [Y.ravel(),
                                     X.ravel()]).T).reshape(
                                (gridsize, 
                                 gridsize))
                        ax[row, column].imshow(
                            np.rot90(dist.T), 
                            extent=[
                                xrange[0], 
                                xrange[-1], 
                                yrange[0], 
                                yrange[-1]], 
                            aspect="auto", 
                            cmap="gray_r")
                        if scatter:
                            ax[row, column].scatter(this_data[:, 1], this_data[:, 0], color="C1", s=5, alpha=0.1, ec=None)
        if filename is not None:
            plt.savefig(filename, bbox_inches="tight", transparent=True)
        return ax